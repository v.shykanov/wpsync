<?php

/**
 * Fired during plugin activation
 *
 * @link       https://site.dev
 * @since      1.0.0
 *
 * @package    Wpsync_Webspark
 * @subpackage Wpsync_Webspark/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wpsync_Webspark
 * @subpackage Wpsync_Webspark/includes
 * @author     dev <dev@mail.dev>
 */
class Wpsync_Webspark_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
