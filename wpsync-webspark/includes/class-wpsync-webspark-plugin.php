<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://site.dev
 * @since      1.0.0
 *
 * @package    Wpsync_Webspark
 * @subpackage Wpsync_Webspark/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wpsync_Webspark
 * @subpackage Wpsync_Webspark/includes
 * @author     dev <dev@mail.dev>
 */
class Wpsync_Webspark_Plugin
{
    protected $api_url = 'https://wp.webspark.dev/wp-api/products?limit=2000';
    protected $api_tries = 10;
    protected $api_interval = 1;
    protected $api_try = 1;
    protected $api_data_cache_file = 'api-products-data.json';
    protected $api_data_chuck_size = 100;

    public function __construct()
    {
        if (!function_exists('wp_crop_image')) {
            require ABSPATH . 'wp-admin/includes/image.php';
        }
    }

    public function wpsync_webspark_activation()
    {
		if ( ! wp_next_scheduled( 'wpsync_webspark_synchronize_event' ) ) {
			wp_schedule_event( time(), 'hourly', 'wpsync_webspark_synchronize_event' );
		}
	}
	
	public function wpsync_webspark_deactivation()
    {
		wp_clear_scheduled_hook( 'wpsync_webspark_synchronize_event' );
	}

    public function wpsync_webspark_synchronize_event()
    {
        $this->wpsync_webspark_synchronize();
    }

	public function wpsync_webspark_register_routes() {
		register_rest_route('wpsync-webspark/v1', '/synchronize/', array(
			'methods'  => WP_REST_Server::READABLE,
			'permission_callback' => function($request) {
				return true;
			},
			'callback' => [$this, 'wpsync_webspark_handler_endpoint_synchronize'],
		) );
        register_rest_route('wpsync-webspark/v1', '/synchronize/chunk/(?P<from>\d+)/(?P<to>\d+)', array(
			'methods'  => WP_REST_Server::READABLE,
			'permission_callback' => function($request) {
				return true;
			},
			'callback' => [$this, 'wpsync_webspark_handler_endpoint_synchronize_chunk'],
		) );
	}
	public function wpsync_webspark_handler_endpoint_synchronize()
	{
        $this->wpsync_webspark_synchronize();
    
        return array(
            'error' => false,
            'status' => 'OK',
        );
	}

    private function products_data_load_api()
    {
        $response = wp_remote_get($this->api_url, array(
            'headers' => array(
                'Accept' => 'application/json',
            ),
        ));

        if ($this->api_try == $this->api_tries) {
            if (is_wp_error( $response )) {
                $error_message = '[wpsync-webspark] API Response error. Message: ' . $response->get_error_message();
            } else {
                $error_message = '[wpsync-webspark] API Response error.';
            }

            error_log($error_message);
            throw new \Exception($error_message);
        }

        if (!$response || is_wp_error($response)) {
            ++$this->api_try;
            sleep($this->api_interval);
            return $this->products_data_load_api();
        }
    
        $body = wp_remote_retrieve_body( $response );
        $data = json_decode( $body, true );
        
        if (json_last_error() > 0) {
            ++$this->api_try;
            sleep($this->api_interval);
            return $this->products_data_load_api();
        }
    
        if (empty($data)) {
            ++$this->api_try;
            sleep($this->api_interval);
            return $this->products_data_load_api();
        }

        if (isset($data['error']) && isset($data['data']) && $data['error'] == false) {
            $this->products_data_cache_store($data['data']);

            return $data['data'];
        }

        ++$this->api_try;
        sleep($this->api_interval);
        return $this->products_data_load_api();
    }

    public function products_data_cache_store($data)
    {
        file_put_contents(WP_CONTENT_DIR . '/' . $this->api_data_cache_file, json_encode($data));
    }

    public function products_data_cache_load()
    {
        $products_cache = WP_CONTENT_DIR . '/' . $this->api_data_cache_file;

        if (!file_exists($products_cache)) {
            return false;
        }

        $data = file_get_contents($products_cache);
        $data = json_decode($data, true);

        return $data;
    }

    public function wpsync_webspark_handler_endpoint_synchronize_chunk($request)
    {
        $params = $request->get_params();

        if (isset($params['from']) && $params['from'] >=0 ) {
            $from = $params['from'];
        } else {
            throw new \Exception('Wrong parameter `from`');
        }

        if (isset($params['to']) && $params['to'] >=0 ) {
            $to = $params['to'];
        } else {
            throw new \Exception('Wrong parameter `to`');
        }

        try {
            $data = $this->products_data_cache_load();

            if (!$data) {
                $data = $this->products_data_load_api();
            }
        } catch (\Exception $exception) {
            $data = $this->products_data_load_api();
        }

        $data = array_slice($data, $from, $to);

        $this->wpsync_webspark_import_products($data);
    }

    public function wpsync_webspark_synchronize_chunks_async_requests($data)
    {
        $requests = [];
        for ($itr = 0; $itr <= count($data); $itr += $this->api_data_chuck_size) {
            $from = $itr;
            $to = $itr+$this->api_data_chuck_size;
            $ip = $_SERVER['SERVER_ADDR'];
            if (isset($_SERVER['REQUEST_SCHEME'])) {
                $scheme = $_SERVER['REQUEST_SCHEME'];
            } else {
                $scheme = 'https';
            }
            $url = sprintf('%s://%s/wp-json/wpsync-webspark/v1/synchronize/chunk/%d/%d/', $scheme, $ip, $from, $to);
            $requests[] = [
                'url' => $url,
	            'type' => 'GET'
            ];
        }

        \WpOrg\Requests\Requests::request_multiple($requests);
    }

    public function wpsync_webspark_synchronize()
    {
        $data = $this->products_data_load_api();
        $this->delete_not_exists_products($data);
        $this->wpsync_webspark_synchronize_chunks_async_requests($data);

        return true;
    }

    public function wpsync_webspark_import_products($data)
    {
        foreach ( $data as $product ) {
            $sku = $product['sku'];
            $name = $product['name'];
            $description = $product['description'];
            $price = $product['price'];
            $picture = $product['picture'];
            $in_stock = $product['in_stock'];
    
            $product_id = wc_get_product_id_by_sku( $sku );
    
            if ( $product_id ) {
                $product = new WC_Product_Simple($product_id);
    
                if ($name != $product->get_name()) {
                    $product->set_name($name);
                }
                if ($description != $product->get_description()) {
                    $product->set_description($description);
                }
    
                if ($price != $product->get_price()) {
                    $product->set_price($price);
                }
    
                if ($in_stock != $product->get_stock_quantity()) {
                    $product->set_stock_quantity($in_stock);
                }
    
                $id = $product->save();
            } else {
                $product = new WC_Product_Simple();
                $image_path = $this->download_and_store_image($sku, $picture);
                
                $type = mime_content_type($image_path);
                $attachment = array(
                    'guid'           => $image_path,
                    'post_mime_type' => $type,
                    'post_title' => $name,
                    'post_content' => '',
                    'post_status' => 'inherit'
                );
                $attach_id = wp_insert_attachment( $attachment, $image_path, $sku);
                $attachment_data = wp_generate_attachment_metadata( $attach_id, $image_path );
                wp_update_attachment_metadata( $attach_id, $attachment_data );
                $product->set_name( $name );
                $product->set_description( $description );
                $product->set_regular_price( $price );
                $product->set_stock_quantity( $in_stock );
                $product->set_sku( $sku );
                $id = $product->save();
                set_post_thumbnail( $id, $attach_id );
            }
        }

        return true;
    }

    private function delete_not_exists_products($data)
    {
        if (!$data) {
            return;
        }

        $existing_products = get_posts(
            array(
                'post_type'      => 'product',
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'fields'         => 'ids',
            )
        );
    
        $existing_skus = array_map(
            function ( $id ) {
                return get_post_meta( $id, '_sku', true );
            },
            $existing_products
        );
    
        $missing_skus = array_diff( $existing_skus, array_column( $data, 'sku' ) );
    
        foreach ( $missing_skus as $sku ) {
            $product_id = wc_get_product_id_by_sku( $sku );
            if ( $product_id ) {
                wp_trash_post( $product_id );
            }
        }
    }

    private function download_and_store_image($sku, $picture)
    {
        $tempfile = tempnam('/tmp', 'wpsync-webspark');
        $result = $this->download_file($picture);
        $data = $result['data'];
        $url = $result['url'];
        file_put_contents($tempfile, $data);
        $revurl = strrev($url);
        list($extension,) = explode('.', $revurl, 2);
        $extension = strrev($extension);
        $upload_dir = wp_upload_dir();
        $to = sprintf('%s/%s.%s', $upload_dir['path'], $sku, $extension);
        copy($tempfile, $to);

        return $to;
    }


    private function download_file($url)
    {
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $data = curl_exec($ch);
        $redirect_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        
        curl_close($ch);

        return [
            'data' => $data,
            'url' => $redirect_url,
            'type' => $content_type,
        ];
    }
}