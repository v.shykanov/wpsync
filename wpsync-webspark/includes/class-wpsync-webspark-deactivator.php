<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://site.dev
 * @since      1.0.0
 *
 * @package    Wpsync_Webspark
 * @subpackage Wpsync_Webspark/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wpsync_Webspark
 * @subpackage Wpsync_Webspark/includes
 * @author     dev <dev@mail.dev>
 */
class Wpsync_Webspark_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
