## Installation

```
cp env.example .env
docker-compose up -d
docker-compose exec wp bash
chmod +x wp
./wp --allow-root core install --url=localhost:89 --title="localhost" --admin_name=admin --admin_password=123123 --admin_email=admin@localhost.dev
./wp --allow-root plugin install woocommerce
./wp --allow-root plugin activate woocommerce
./wp --allow-root plugin activate wpsync-webspark
./wp --allow-root rewrite structure '/%postname%'
```

[Test host](http://143.47.185.88)